package Quadrado;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;


import Quadrado.PainelQuadrado;

public class UniversoQuadrado extends JFrame{
	Quadrado t;

	public UniversoQuadrado (int altura, int largura, Quadrado t) {
		this.t = t;

		PainelQuadrado i = new PainelQuadrado(100, 100, altura, largura, t);

		add(i);
		JButton rotacionar = new JButton("Rotacionar");
		rotacionar.setSize(100, 50);
		i.add(rotacionar);
		JButton transladar = new JButton("Transladar");
		transladar.setSize(100, 50);
		i.add(transladar);
		JButton escalar = new JButton("Escalar");
		escalar.setSize(100, 50);
		i.add(escalar);
		
		JButton reflexao = new JButton("Reflexão");
		rotacionar.setSize(100, 50);
		i.add(reflexao);
		
		
		rotacionar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == rotacionar) {
					t.rotacao(90);
					i.setT(t);
					i.repaint();
				}

			}
		});

		transladar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == transladar) {
					t.translacao(100, 100);
					i.setT(t);
					i.repaint();
				}

			}
		});
		escalar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == escalar) {
					t.escala(2, 2);
					i.setT(t);
					i.repaint();
				}

			}
		});
		
	
		

	}
}
