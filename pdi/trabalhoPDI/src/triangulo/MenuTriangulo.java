package triangulo;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fuguras.Ponto;
import fuguras.Triangulo;

public class MenuTriangulo extends JFrame {
	public MenuTriangulo() {
		JPanel painel = new JPanel();
		painel.setLayout(new FlowLayout());
		JTextField p1x, p1y, p2x, p2y, p3x, p3y;
		JButton done;
		JLabel pontoUmX = new JLabel("Ponto 1 X");
		JLabel pontoUmY = new JLabel("Ponto 1 Y");
		JLabel pontoDoisX = new JLabel("Ponto 2 X");
		JLabel pontoDoisY = new JLabel("Ponto 2 Y");
		JLabel pontoTresX = new JLabel("Ponto 3 X");
		JLabel pontoTresY = new JLabel("Ponto 3 Y");
		p1x = new JTextField(10);
		p1y = new JTextField(10);
		p2y = new JTextField(10);
		p2x = new JTextField(10);
		p3y = new JTextField(10);
		p3x = new JTextField(10);
		done = new JButton("Done");
		painel.add(pontoUmX);
		painel.add(p1x);
		painel.add(pontoUmY);
		painel.add(p1y);
		painel.add(pontoDoisX);
		painel.add(p2x);
		painel.add(pontoDoisY);
		painel.add(p2y);
		painel.add(pontoTresX);
		painel.add(p3x);
		painel.add(pontoTresY);
		painel.add(p3y);
		painel.add(done);
		add(painel);

		done.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == done) {
					
					Ponto um = new Ponto();
					Ponto dois = new Ponto();
					Ponto tres = new Ponto();
					
					um.setX((Integer.parseInt(p1x.getText())*100)+100);
					dois.setX((Integer.parseInt(p2x.getText())*100)+100);
					tres.setX((Integer.parseInt(p3x.getText())*100)+100);
					
					um.setY((Integer.parseInt(p1y.getText())*100)+100);
					dois.setY((Integer.parseInt(p2y.getText())*100)+100);
					tres.setY((Integer.parseInt(p3y.getText())*100)+100);
					
									
					Triangulo t = new Triangulo(um,dois,tres);
					System.out.println(t.getUm().getX());
					DefinirTamanhoUniversoTriangulo menu = new DefinirTamanhoUniversoTriangulo(t);
					menu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					menu.setSize(600, 400);
					menu.setVisible(true);

				}

			}
		});

	}
}
