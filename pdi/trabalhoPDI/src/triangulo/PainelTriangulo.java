package triangulo;

import java.awt.Graphics;

import javax.swing.JPanel;

import fuguras.Ponto;
import fuguras.Triangulo;

public class PainelTriangulo extends JPanel {
	
	int a,b,c,d;
	Triangulo t;
	public PainelTriangulo (int a ,int b, int c, int d, Triangulo t){
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
		this.t = t;
	}
	
	public Triangulo getT() {
		return t;
	}

	public void setT(Triangulo t) {
		this.t = t;
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);		
		g.drawRect(a,b,c,d);
//		Ponto um = t.getUm();
//		Ponto dois = t.getDois();
//		Ponto tres = t.getTres();
		
		
		Ponto um = t.getUm();
		Ponto dois = t.getDois();
		Ponto tres = t.getTres();
		

		
	    g.drawLine((int)um.getX(),(int)um.getY(), (int)dois.getX(),(int) dois.getY());
	    g.drawLine((int)dois.getX(),(int)dois.getY(), (int)tres.getX(),(int) tres.getY());
	    g.drawLine((int)um.getX(),(int)um.getY(), (int)tres.getX(),(int) tres.getY());
		
	}
	void desenharLinha(Graphics g, int xP, int yP, int xQ, int yQ){
		int x = xP, y = yP;
		float d = 0;
		System.out.println(xP+yP);
		float m = Math.abs((float)(yQ-yP))/Math.abs((float)(xQ-xP));
		for(;;){
			putPixel(g,x,y);
			if(x == xQ)	break;
			x++;
			d+=m;
			if(d>=0.5){
				y++;
				d--;
			}
		}
	}
	void putPixel(Graphics g, int x, int y){
		g.drawLine(x, y, x, y);
	}
	
}
