import java.util.ArrayList;

public class Figura {	
	private ArrayList<Ponto> pontos = new ArrayList<>();
	public ArrayList<Ponto> getLinhas() {
		return pontos;
	}
	public void setLinhas(ArrayList<Ponto> linhas) {
		this.pontos = linhas;
	}
	public void adicionarFigura(Ponto ponto){
		pontos.add(ponto);
	}
}
