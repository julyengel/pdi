
public class Ponto {
	float x;
	float y;

	public Ponto() {
		
	}
	public Ponto(float x,float y){
		this.x = x;
		this.y = y;
		
	}
	public float getX() {
		return x;
	}

	public void setX(float x1) {
		this.x = x1;
	}

	public float getY() {
		return y;
	}

	public void setY(float y2) {
		this.y = y2;
	}
}
