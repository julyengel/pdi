
public class Triangle {
	private Point2D C;
	private Point2D B;
	private Point2D A;

	public Triangle(Point2D A, Point2D B, Point2D C) {
		this.setA(A);
		this.setB(B);
		this.setC(C);
	}

	public float area() {
		return Math.abs(
				((A.getX() - C.getX()) * (B.getY() - C.getY()) - (A.getY() - C.getY()) * (B.getX() - C.getX())) / 2);
	}

	public void setA(Point2D a) {
		A = a;
	}

	public Point2D getA() {
		return A;
	}

	public void setB(Point2D b) {
		B = b;
	}

	public Point2D getB() {
		return B;
	}

	public void setC(Point2D c) {
		C = c;
	}

	public Point2D getC() {
		return C;
	}
}
