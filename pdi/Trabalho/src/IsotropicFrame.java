
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.List;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.JFrame;

@SuppressWarnings("serial")
public class IsotropicFrame extends JFrame {
	/**
	 * @param args
	 */

	public static void main(String[] args) {
		new IsotropicFrame();
	}

	IsotropicFrame() {
		super("Modo de mapeamento isotrópico.");
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});

		setSize(400, 300);
		add("Center", new CvIsotropic());
		setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		setVisible(true);
	}
}

@SuppressWarnings("serial")
class CvIsotropic extends Canvas {
	int centerX;
	int centerY;
	float pixelSize;
	float rWidth = 10F;
	float rHeight = 10F;
	float xP = 1000000;
	float yP;
	int numPontos = 0;
	Point2D um = new Point2D();
	Point2D dois = new Point2D();
	Point2D tres = new Point2D();
	int ponto = 0;
	int x1, y1, x2, y2;
	Line l1, l2, l3;
	int lq;
	ArrayList<Line> linhas = new ArrayList<Line>();

	CvIsotropic() {
		addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				x1 = e.getX();
				y1 = e.getY();
				System.out.println(x1);
				um.setX(x1);
				um.setY(y1);
				numPontos++;
			}

			public void mouseReleased(MouseEvent e) {
				x2 = e.getX();
				y2 = e.getY();
				dois.setX(x2);
				dois.setY(y2);
				l1 = new Line(x1, y1, x2, y2);
				linhas.add(l1);
				repaint();
			}

		});

	}

	void initGr() {
		Dimension d = getSize();
		int maxX = d.width - 1;
		int maxY = d.height - 1;
		pixelSize = Math.max(rWidth / maxX, rHeight / maxY);
		centerX = maxX / 2;
		centerY = maxY / 2;
	}

	int iX(float x) {
		return Math.round(centerX + x / pixelSize);
	}

	int iY(float y) {
		return Math.round(centerY - y / pixelSize);
	}

	float fx(int x) {
		return (x - centerX) * pixelSize;
	}

	float fy(int y) {
		return (centerY - y) * pixelSize;
	}

	public void paint(Graphics g) {
		int i = 0;
		while (i < linhas.size()) {
			int l1, l2, l3, l4;

			g.drawLine((int) linhas.get(i).getX1(), (int) linhas.get(i).getY1(), (int) linhas.get(i).getX2(),
					linhas.get(i).getY2());
		}

	}
}