
public class Point2D {
	private float y;
	private float x;

	public Point2D(float x, float y) {
		this.setX(x);
		this.setY(y);
	}
	public Point2D() {
		
	}
	public void setX(float x) {
		this.x = x;
	}

	public float getX() {
		return x;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float getY() {
		return y;
	}
}
