import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class Painel extends JPanel {
	
	int a,b,c,d;
	
	public Painel(int a ,int b, int c, int d){
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);		
		g.drawRect(a,b,c,d);
		
	}
}
