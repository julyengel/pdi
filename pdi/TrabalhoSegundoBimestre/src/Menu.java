import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Menu extends JFrame {
	
	public static void main(String[] args) {
		Menu f = new Menu();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(600, 400);
		f.setVisible(true);

	}
	
	
		public Menu(){
			JPanel painel = new JPanel();
			painel.setLayout(new FlowLayout());
			JButton quadrado = new JButton("Quadrado");
			JButton triangulo = new JButton("Triangulo");
			JButton linha = new JButton("Linha");
			painel.add(quadrado);
			painel.add(triangulo);
			painel.add(linha);
			add(painel);
			
			
		triangulo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == triangulo) {
					MenuTrinagulo menu = new MenuTrinagulo();
					menu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					menu.setSize(400, 400);
					menu.setVisible(true);
				}

			}
		});
			
		}
}
