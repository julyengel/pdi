public class Triangulo {
	Ponto2D um;
	Ponto2D dois;
	Ponto2D tres;

	public Ponto2D getUm() {
		return um;
	}

	public void setUm(Ponto2D um) {
		this.um = um;
	}

	public Ponto2D getDois() {
		return dois;
	}

	public void setDois(Ponto2D dois) {
		this.dois = dois;
	}

	public Ponto2D getTres() {
		return tres;
	}

	public void setTres(Ponto2D Tres) {
		this.tres = Tres;
	}

	public void rotacao(int angulo) {
		float pi = (float) ((3.14) * angulo / 180);
		float x1, x2, x3;
		float y1, y2, y3;
		x1 = (float) Math.abs(um.getX() * Math.cos(pi) - um.getY() * Math.sin(pi));
		y1 = (float) Math.abs(um.getX() * Math.sin(pi) - um.getY() * Math.cos(pi));
		x2 = (float) Math.abs(dois.getX() * Math.cos(pi) - dois.getY() * Math.sin(pi));
		y2 = (float) Math.abs(dois.getX() * Math.sin(pi) - dois.getY() * Math.cos(pi));
		x3 = (float) Math.abs(tres.getX() * Math.cos(pi) - dois.getY() * Math.sin(pi));
		y3 = (float) Math.abs(tres.getX() * Math.sin(pi) - tres.getY() * Math.cos(pi));

		um.setX(x1);
		um.setY(y1);
		dois.setX(x2);
		dois.setY(y2);
		tres.setX(x3);
		tres.setY(y3);
	}

	public void translacao(int fx, int fy) {
		float x1, x2, x3;
		float y1, y2, y3;
		x1 = um.getX() + fx;
		y1 = um.getY() + fy;
		x2 = dois.getX() + fx;
		y2 = dois.getY() + fy;
		x3 = tres.getX() + fx;
		y3 = tres.getY() + fy;
		um.setX(x1);
		um.setY(y1);
		dois.setX(x2);
		dois.setY(y2);
		tres.setX(x3);
		tres.setY(y3);
	}

	public void escala(int x, int y) {
		float x1, x2, x3;
		float y1, y2, y3;

		x1 = um.getX() * x;
		y1 = um.getY() * y;
		x2 = dois.getX() * x;
		y2 = dois.getY() * y;
		x3 = tres.getX() * x;
		y3 = tres.getY() * y;
		
		um.setX(x1);
		um.setY(y1);
		dois.setX(x2);
		dois.setY(y2);
		tres.setX(x3);
		tres.setY(y3);
	}

public Triangulo(Ponto2D um, Ponto2D dois, Ponto2D tres) {
		super();
		this.um = um;
		this.dois = dois;
		this.tres = tres;
	}
public void reflexao(Ponto2D um, Ponto2D dois, Ponto2D tres){
	float y2;
	y2 = dois.getY()*(-1);
	dois.setY(y2);

}
}