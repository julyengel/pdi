import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.xml.bind.ParseConversionEvent;

public class FirstWindow extends JFrame {
	Triangulo t;
	public FirstWindow(Triangulo t) {
		this.t= t;
		JPanel painel = new JPanel();
		JTextField blargura, baltura;
		blargura = new JTextField(20);
		baltura = new JTextField(20);
		JLabel llargura = new JLabel("Largura: ");
		JLabel laltura = new JLabel("Altura: ");

		JButton feito;
		feito = new JButton("Done");
		painel.setLayout(new FlowLayout());
		painel.add(llargura);
		painel.add(blargura);
		painel.add(laltura);
		painel.add(baltura);
		painel.add(feito);
		this.add(painel);
		feito.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == feito) {
					int altura = (Integer.parseInt(baltura.getText())*100)+100;
					int largura = (Integer.parseInt(blargura.getText())*100)+100;
					Universo universo = new Universo(altura,largura, t);
					universo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					universo.setSize(altura+400, largura+400);
					universo.setVisible(true);
				}

			}
		});

	}

}
